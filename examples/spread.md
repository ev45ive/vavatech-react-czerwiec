```ts
obj1 = { a:1 , b:2 }
obj2 = { a:'x',     c:'z'}

obj3 = { ...obj1, ...obj2, d:'placki'}
{a: "x", b: 2, c: "z", d: "placki"}
list1 = [1,2,3]
list2 = ['x','y','z']
list3 = [ ...list1, ...list2, 'placki' ]
(7) [1, 2, 3, "x", "y", "z", "placki"]
