```ts

function useState(){   
    ctx = getCurrentTreePosition()

    return [ 
         ctx.state,
         function(nextState){
            ctx.state = state;
            React.render( AComponent(state) , ctx)
         }
    ]
}

function AComponent(){
    const [state,changeState] = useState()

    return {
        type:'div', 
        props:{
            onClick(){
                changeState( state +1 )
            }
        }
    }
}