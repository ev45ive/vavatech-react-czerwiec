```tsx

window.React = React
window.ReactDOM = ReactDOM

interface Person {
  name: string;
  pet: string;
  color: string;
}

// const person: Person = { name: 'Alice', pet: 'kota', color: 'blue' }
const people: Person[] = [
  { name: 'Alice', pet: 'kota', color: 'blue' },
  { name: 'Bob', pet: 'psa', color: 'red' },
  { name: 'Kate', pet: 'papugi', color: 'green' },
]

// function Person(person: Person) {
//   return React.createElement('li', {
//     id: 123, title: 'person', className: 'testclass', key: person.name
//   },
//     React.createElement('p', {
//       // style: 'color:red ' //// error
//       style: { color: person.color }
//     },
//       person.name + ' ma ' + person.pet + ' i ',
//       React.createElement('input')
//     )
//   )
// }

type Props = { person: Person, costam?: any }
// function PersonCard(props: { person: Person, costam?: any }) {

function PersonCard( { person, costam }: Props) {
  // const person = props.person;
//   const { person } = props;

  return <div id="123" title={person.name} className="testcalss">
    <p style={{ color: person.color }}>
      {person.name} ma {person.pet}
      <input />
    </p>
  </div>
}

const state = {
  people: people
}

function update(state: { people: Person[]; }) {
  const vdiv = <ul>
    {state.people.map(p => <li key={p.name}>
      <PersonCard person={p} costam="" />
    </li>)}
  </ul>
  ReactDOM.render(vdiv, document.getElementById('root'))

  // const peopleElems = state.people.map(p => Person(p))
  // const vdiv = React.createElement('ul', null,
  //   peopleElems
  // )
  // const vdiv = <ul>
  //   {state.people.map(p => <li key={p.name}>
  //     {PersonCard({ person: p })}
  //   </li>)}
  // </ul>

  // ReactDOM.render(vdiv, document.getElementById('root'))
}

setInterval(() => {
  state.people.unshift(state.people.pop()!)
  update(state)
}, 1000)

// const vdiv = React.createElement('ul', null,
//   Person(people[0]),
//   Person(people[1]),
//   Person(people[2]),
// )
```