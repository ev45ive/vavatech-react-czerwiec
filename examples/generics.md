```ts
function getFirst<T>(arr: T[]): T {
  return arr[0];
}
const first1 = getFirst<string>(["123"]);
const first2 = getFirst(playlists);
const first3: number = getFirst([123]);

const getFirst1 = <T>(arr: T[]): T => {
  return arr[0];
};

const getFirst2: <T>(arr: T[]) => T = (arr) => {
  return arr[0];
};
```
