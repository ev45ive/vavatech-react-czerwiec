```js

SyntheticBaseEvent
 event.currentTarget !== event.nativeEvent.currentTarget

// Parent

buttonsList.addEventListener('click', function(event){
    if(!event.target.closest('.select-button')){ return }
    event    
    event.target
    event.currentTarget

    button = event.target.closest('[data-playlist-id]')
    data = button.dataset
    debugger
})

// Child

buttonsList.addEventListener('click', function(event){
    if(!event.target.closest('.delete-button')){ return }
    
    // Stop propagating to parent
    event.stopPropagation()
    
    button = event.target.closest('[data-playlist-id]')
    data = button.dataset
    // delete(data.playlistId)
    debugger
})
