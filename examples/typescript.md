##  Structural vs Nominal types
```ts
interface Vector { x: number, y: number, length:number }
interface Point { x: number, y: number }

let v: Vector = { x: 123, y: 324, length:123 }
let p: Point = { x: 123, y: 324 }

p = v
// v = p  // Property 'length' is missing in type 'Point' but required in type 'Vector'

```

## Extract types
```ts
export interface SimpleAlbum {
    id:                     Album['id'];
    images:                 Album['images'];
    name:                   Album['name'];
}

```

## Indexed types
```ts

interface Dictionary {
    [aKey: string]: { value: string }
}
const dict: Dictionary = { abc: { value: '23' } }
// interface Params{
//     // An index signature parameter type cannot be a union type. Consider using a mapped object type instead.
//     [key: 'ParamA' | 'ParamB']: string
// }

type Params = {
    // An index signature parameter type cannot be a union type. Consider using a mapped object type instead.
    [key in 'ParamA' | 'ParamB']: string
}
// type Params = {} // No declaration merging // Error
const params:Params = {ParamA:'A',ParamB:'B'}

```

## Type Mapping
```ts

type SimpleAlbum1 = {
    [key in 'id' | 'images' | 'name']: Album[key]
}

// type AlbumKeys =  'id' | 'images' | 'name'
type AlbumKeys = keyof Album
// const k: AlbumKeys = 'images'

type SimpleAlbum2 = {
    // [key in AlbumKeys]: string
    // [key in AlbumKeys]?: Album[key]
    readonly [key in AlbumKeys]: Album[key]
}
// \node_modules\typescript\lib\lib.es5.d.ts
// type Partial<T> = {
//     [key in keyof T]?: T[key]
// }
const mock: Partial<Album> = { id: '123', name: '123', images: [] }


type SimpleAlbum = Pick<Album, 'id' | 'name'> & {
    // images: Pick<Image,'url'>[]
    images: Pick<Album['images'][0], 'url'>[]
}

const mock1: SimpleAlbum = {
    id: '123', name: '', images: [{ url: '' }]
}

type SomeKeys1 = Extract<'test' | 'costam' | 123 | undefined, string>
type SomeKeys2 = Exclude<'test' | 'costam' | 123 | undefined, string>

const mock: Partial<Album> = { id: '123', name: '123', images: [] }


```

## Template literals
```ts

type options = `OPTION_${ 'A' | 'B' | 'C'}` //  "OPTION_A" | "OPTION_B" | "OPTION_C"
```

## Declaration Merging
```ts
// some-library/index.ts
namespace LibraryX{
    export interface RequestData{ name:string }
}

// some-plugin/index.ts
// interface SpecialRequestData extends RequestData{special:number}
namespace LibraryX{
    export interface RequestData{ id:number }
}

// my-app/index.ts
// const d: SpecialRequestData = { name:'test' }
const d: LibraryX.RequestData = { name:'test', id:123 }
```