```js
function WhatIsThis(){
    console.log(this) 
}

function Person(name){
    ////const person = {}
    this.name = name;
    
    // return person;
}

// Object-Oriented with functions (Look Ma! no classes!)
alice = {}
Person.apply(alice,['Alice'])
alice;

function Person(name){
    this.name = name;
//     this.sayHello = function(){
//         console.log(`Hi, I am ${this.name}`)
//     } // Wrong `this`

//     this.sayHello = function(){
//         console.log(`Hi, I am ${this.name}`)
//     }.bind(this) // OK 
    
    this.sayHello = () => {
        console.log(`Hi, I am ${this.name}`)
    } // BEtter!
}

alice = new Person('Alice') 
alice instanceof Person // true
alice.sayHello()
// Hi, I am Alice

b = document.createElement('button')
b.name = "aButton"
b.onclick = alice.sayHello
b.click()
// Hi, I am Alice

new (() => {} ) // VM5003:1 Uncaught TypeError: (intermediate value) is not a constructor