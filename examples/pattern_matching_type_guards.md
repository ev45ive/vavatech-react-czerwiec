```ts

let possiblyUndefined = '12' as string | undefined

// Type Guard
if (possiblyUndefined !== undefined) {
    possiblyUndefined.toLocaleUpperCase()
}

type AcceptableValues = string | number | undefined // | boolean;

function convertValue(maybeStringOrNumber: AcceptableValues) {
    if (typeof maybeStringOrNumber === 'number') {
        return maybeStringOrNumber.toFixed(2)
    } else if (typeof maybeStringOrNumber === 'string') {
        return maybeStringOrNumber.toLocaleUpperCase()
    } 
    
    if (typeof maybeStringOrNumber === 'undefined') {
        // maybeStringOrNumber
        return 'Nothing'
    } else {
        // const _never: never = maybeStringOrNumber
        // throw new Error('Unexpected value ' + maybeStringOrNumber)
        checkExhaustivenes(maybeStringOrNumber)
        // const this_will_never_happen_either = 1 // Unreachable code detected.
    }
}

function checkExhaustivenes(_never: never): never {
    throw new Error('Unexpected value ' + _never)
}


```
## Pattern matching helpers
https://github.com/gvergnaud/ts-pattern