import { addDecorator } from '@storybook/react';
import { initializeWorker, mswDecorator } from 'msw-storybook-addon';

initializeWorker();
// addDecorator(mswDecorator);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },

}

import 'bootstrap/dist/css/bootstrap.css';

export const decorators = [
  mswDecorator,
  (Story) => {
    return (
      <div className="container">
        <Story />
      </div>

      // <div style={{ margin: '3em' }}>
      //   <Story />
      // </div>
    )
  },
]