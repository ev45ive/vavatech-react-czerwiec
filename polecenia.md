## GIT
cd ..
git clone https://bitbucket.org/ev45ive/vavatech-react-czerwiec.git vavatech-react-czerwiec
cd vavatech-react-czerwiec
npm i 
code .
npm start

## GIT Update
git stash -u && git pull 
## Instalacje / wersje
node -v
v14.17.0

npm -v 
6.14.6

code -v 
1.57.0

git --version
git version 2.31.1.windows.1

chrome

npx create-react-app --help

npx create-react-app --version
npx create-react-app --version
4.0.3

## VSCode extensions
Emmet (builtin)
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets


## Chrome extensions
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

## New project
npx create-react-app . --template typescript
npm start


## API
https://developer.spotify.com/documentation/web-api/reference/


## Playlists

mkdir -p src/playlists/containers
mkdir -p src/playlists/components
touch src/playlists/containers/PlaylistsView.tsx
touch src/playlists/components/PlaylistsList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditForm.tsx


## Music Search
mkdir -p src/music-search/containers
mkdir -p src/music-search/components
touch src/music-search/containers/MusicSearchView.tsx
touch src/music-search/components/SearchForm.tsx
touch src/music-search/components/SearchResults.tsx
touch src/music-search/components/AlbumCard.tsx


## UI toolkits

https://bradfrost.com/blog/post/atomic-web-design/
https://x-team.com/blog/best-react-component-libraries/


https://material-ui.com/styles/basics/
https://react-bootstrap.github.io/components/input-group/
https://ant.design/docs/react/introduce


https://blueprintjs.com/docs/#core/components/card

### Enterprise
https://js.devexpress.com/Documentation/Guide/React_Components/DevExtreme_React_Components/ 
https://primefaces.org/primereact/showcase/#/dataview
https://www.telerik.com/kendo-react-ui/ 


## Headless (no CSS)
https://headlessui.dev/react/menu 

# Storybook ( design system / styleguide )

https://storybook.js.org/docs/react/get-started/install

npx sb init

## Stories 
https://storybook.js.org/docs/react/essentials/controls
https://storybook.js.org/docs/riot/workflows/build-pages-with-storybook

## Mockin server
https://developers.google.com/web/fundamentals/primers/service-workers
https://developers.google.com/web/tools/workbox/guides/get-started


https://mswjs.io/
https://www.npmjs.com/package/msw-storybook-addon
### Instalacja addona
npm i -D msw msw-storybook-addon   
npm i --save-dev @types/msw-storybook-addon 
### Generujemy service worker (skrypt proxy)
npx msw init public/

Initializing the Mock Service Worker at "C:\Projects\szkolenia\vavatech-react-czerwiec\public"...

Service Worker successfully created!
C:\Projects\szkolenia\vavatech-react-czerwiec\public\mockServiceWorker.js

Continue by creating a mocking definition module in your application:

https://mswjs.io/docs/getting-started/mocks

INFO In order to ease the future updates to the worker script,
we recommend saving the path to the worker directory in your package.json.

? Do you wish to save "public" as the worker directory? Yes
Writing "msw.workerDirectory" to "C:\Projects\szkolenia\vavatech-react-czerwiec\package.json"...

## e2e Cypress Testing
https://www.cypress.io/
https://swagger.io/ 


## Backend frontend integration
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype
https://swagger.io/ 
https://github.com/graphql/graphiql

## Spotify OAuth 
https://developer.spotify.com/documentation/general/guides/authorization-guide/


mkdir -p src/core/services
touch src/core/services/AuthService.ts

## Class components - PlaylistTracksView

touch src/playlists/containers/PlaylistTracksView.tsx
touch src/playlists/components/PlaylistSelector.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/TracksList.tsx


## Wzorce kompozycji komponentów (odzielamy stan i render)

https://pl.reactjs.org/docs/higher-order-components.html
https://pl.reactjs.org/docs/render-props.html
https://kentcdodds.com/blog/how-to-give-rendering-control-to-users-with-prop-getters
https://pl.reactjs.org/docs/hooks-overview.html#building-your-own-hooks


## Testowanie
https://pl.reactjs.org/docs/testing.html
https://jestjs.io/

https://pl.reactjs.org/docs/testing-environments.html#mocking-a-rendering-surface
https://github.com/jsdom/jsdom

https://testing-library.com/docs/react-testing-library/intro/ 
https://pl.reactjs.org/docs/testing-recipes.html

npm run test
No tests found related to files changed since last commit.    
Press `a` to run all tests, or run Jest with `--watchAll`.    

Watch Usage
 › Press a to run all tests.
 › Press f to run only failed tests.
 › Press q to quit watch mode.
 › Press p to filter by a filename regex pattern.
 › Press t to filter by a test name regex pattern.
 › Press Enter to trigger a test run.

## Selectors
https://testing-library.com/docs/react-testing-library/cheatsheet
	        No Match	1 Match	    1+ Match	Await?
getBy	    throw	    return	    throw	    No
findBy	    throw	    return	    throw	    Yes
queryBy	    null	    return	    throw	    No
getAllBy	throw	    array	    array	    No
findAllBy	throw	    array	    array	    Yes
queryAllBy	[]	        array	    array	    No

## Use ARIA / WCAG Accesibility for selectors

https://material-ui.com/components/tabs/
https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/Tab_Role


## Validation / Parsing
https://basarat.gitbook.io/typescript/type-system/typeguard

https://github.com/react-hook-form/resolvers

https://github.com/gcanti/io-ts/blob/master/index.md
https://github.com/jquense/yup/blob/master/docs/typescript.md
https://github.com/typestack/class-validator

https://github.com/colinhacks/zod#unions
https://opensourcelibs.com/lib/zod // password match .refine(...)
https://react-hook-form.com/get-started#SchemaValidation

https://github.com/gvergnaud/ts-pattern

## CSS in JS
https://cssinjs.org/?v=v10.6.0
https://cssinjs.org/react-jss/?v=v10.0.0-alpha.10
https://styled-components.com/

npm i --save styled-components
npm i --save-dev @types/styled-components

Themes and design systems
https://material-ui.com/system/basics/

## React router
npm install react-router-dom @types/react-router-dom


## Snapshot testing
https://jestjs.io/docs/snapshot-testing


## Forms
<!-- useState: -->
https://formik.org/docs/guides/validation 

<!-- useRef -->
https://react-hook-form.com/get-started


https://react-hook-form.com/api/usefieldarray
https://react-hook-form.com/api/useformcontext
https://react-hook-form.com/advanced-usage#WizardFormFunnel


https://react-hook-form.com/api/usecontroller/controller
http://ericgio.github.io/react-bootstrap-typeahead/#asynchronous-searching
https://github.com/react-hook-form/react-hook-form/discussions/2624 

## Validation
https://react-hook-form.com/get-started#SchemaValidation

npm install @hookform/resolvers  zod

https://react-hook-form.com/advanced-usage#CustomHookwithResolver


## Redux
https://redux.js.org/
https://react-redux.js.org/
https://redux-toolkit.js.org/

npm install redux react-redux @reduxjs/toolkit

## Redux middleware
logger,
thunk,
https://github.com/pburtchaell/redux-promise-middleware/blob/HEAD/docs/introduction.md
https://redux-saga.js.org/docs/recipes

https://redux-saga.js.org/docs/advanced/TaskCancellation

