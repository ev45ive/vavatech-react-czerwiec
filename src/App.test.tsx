import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

import "./core/services/AuthService";
import { Router, MemoryRouter, useHistory } from "react-router-dom";
import { createMemoryHistory } from "history";
import { act } from "react-test-renderer";

import { MusicSearchView } from "./music-search/containers/MusicSearchView";
import PlaylistTracksView from "./playlists/containers/PlaylistTracksView";
import { PlaylistsTDDView } from "./playlists/containers/PlaylistsTDDView";

jest.mock("./music-search/containers/MusicSearchView", () => ({
  MusicSearchView: () => "MusicSearchView",
}));

jest.mock("./playlists/containers/PlaylistTracksView", () => ({
  default: () => "PlaylistTracksView",
  __esModule: true, // this property makes it work
}));

jest.mock("./playlists/containers/PlaylistsTDDView", () => ({
  PlaylistsTDDView: () => "PlaylistsTDDView",
}));

jest.mock("./core/services/AuthService");

jest.mock("react-router-dom", () => {
  return {
    ...jest.requireActual("react-router-dom"),
    useHistory: () => ({
      push: jest.fn(),
    }),
  };
});

test("renders App", () => {
  // const history = createMemoryHistory({});
  act(() => {
    render(
      // <Router history={history}>
      //   <App />
      // </Router>

      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
  });

  // history.push()

  const linkElement = screen.getByText(/MusicApp/i);
  expect(linkElement).toBeInTheDocument();
});
