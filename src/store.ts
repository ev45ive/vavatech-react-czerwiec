import { createStore, combineReducers, applyMiddleware, Middleware, AnyAction, Dispatch, MiddlewareAPI } from 'redux'
import * as fromSearch from './core/reducers/MusicSearchReducer'
import * as fromCounter from './core/reducers/CounterReducer'
import * as fromPlaylists from './core/reducers/PlaylistsReducer'
import logger from 'redux-logger'
import thunk, { ThunkDispatch } from 'redux-thunk'
import { configureStore } from '@reduxjs/toolkit'
import { useDispatch, TypedUseSelectorHook, useSelector } from 'react-redux'

export const store = configureStore({
    reducer: {
        [fromSearch.featureKey]: fromSearch.reducer,
        [fromCounter.counterSlice.name]: fromCounter.default,
        [fromPlaylists.featureKey]: fromPlaylists.default
    },
    middleware: [thunk, logger]
})

export type AppState = ReturnType<typeof store.getState>
export type AppDispatch = Parameters<typeof store.dispatch>[0]
export const useAppDispatch = () => useDispatch<Dispatch<AppDispatch | any>>()
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector


    // const reducer = combineReducers({
    //     [fromSearch.featureKey]: fromSearch.reducer
    // })
    // export const store = createStore(reducer, applyMiddleware(
    //     thunk, logger
    // ))


    // export type AppState = ReturnType<typeof reducer>


    ; (window as any).store = store
