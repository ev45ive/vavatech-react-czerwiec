import React from "react";
import ReactDOM from "react-dom";
// import { HashRouter as Router } from 'react-router-dom';
import { BrowserRouter as Router } from "react-router-dom";
import { DefaultTheme, ThemeProvider } from "styled-components";
import App from "./App";
import { UserProvider } from "./core/providers/UserProvider";
import { initAuth } from "./core/services/AuthService";
// import './index.css';
import reportWebVitals from "./reportWebVitals";
import { store } from "./store";
import { Provider } from "react-redux";
declare module "styled-components" {
  interface DefaultTheme {
    primary: {
      color: string;
    };
  }
}

const globalTheme: DefaultTheme = {
  primary: {
    color: "#ccc",
  },
};

initAuth();

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={globalTheme}>
      <UserProvider>
        <Provider store={store}>
          <Router>
            <App />
          </Router>
        </Provider>
      </UserProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
