import React, { useEffect, useRef, useState } from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
    playlist: Playlist,
    onCancel(): void
    onSave(draft: Playlist): Promise<string | null>
}


export const PlaylistEditForm = React.memo(({ playlist, onCancel, onSave }: Props) => {
    const [playlistName, setPlaylistName] = useState(playlist.name);
    const [isPublic, setIsPublic] = useState(playlist.public);
    const [description, setDescription] = useState(playlist.description)
    const [confirmBoxOpen, setConfirmBoxOpen] = useState(false)
    const [changesConfirmed, setChangesConfirmed] = useState<undefined | boolean>(undefined)
    const [message, setMessage] = useState<string | null>(null)


    useEffect(() => {
        setConfirmBoxOpen(true)
    }, [playlist])

    useEffect(() => {
        if (changesConfirmed === true) {
            setPlaylistName(playlist.name)
            setIsPublic(playlist.public)
            setDescription(playlist.description)
        }
        setChangesConfirmed(undefined)
        setConfirmBoxOpen(false)
    }, [changesConfirmed])

    const submit = async () => {
        const draft: Playlist = {
            ...playlist,
            name: playlistName,
            public: isPublic,
            description,
        }
        const result = await onSave(draft)
        setMessage(result)
    }

    // const ref = useRef('placki')
    const nameInputRef = useRef<HTMLInputElement | null>(null)

    useEffect(() => {
        // const playlist_name = document.getElementById('playlist_name')
        // playlist_name?.focus()
        // const elem = nameInputRef.current // Dont do this! Wont update
        nameInputRef.current?.focus()
    }, [])

    return (
        <div data-testid="playlist-form">
            {/* <pre>{JSON.stringify(playlist, null, 2)}</pre>
            <pre>{JSON.stringify({
                name: playlistName,
                public: isPublic,
                description,
            }, null, 2)}</pre> */}

            {confirmBoxOpen && <div className="alert alert-danger">
                Changes will be lost. Are you sure?
                <button className="float-end btn btn-danger btn-sm" onClick={() => setChangesConfirmed(true)}>Yes</button>
                <button className="float-end btn btn-success btn-sm" onClick={() => setChangesConfirmed(false)}>No</button>
            </div>}

            {message && <p className="alert alert-danger">
                {message}
            </p>}

            <div className="form-group mb-3">
                <label htmlFor="playlist_name">Name:</label>
                <input
                    type="text"
                    className="form-control"
                    id="playlist_name"
                    ref={nameInputRef}
                    // ref={elem => console.log(elem)}
                    aria-describedby="helpId"
                    value={playlistName}
                    onChange={(event) => { setPlaylistName(event.currentTarget.value); }}
                />

                <small id="helpId" className="form-text text-muted">
                    {playlistName.length} / 170
                </small>
            </div>

            <div className="form-check mb-3">
                <label className="form-check-label">
                    <input
                        type="checkbox"
                        className="form-check-input"
                        value="checkedValue"
                        checked={isPublic}
                        onChange={(e) => setIsPublic(e.currentTarget.checked)}
                    />{" "}
                    Public
                </label>
            </div>

            <div className="form-group mb-3">
                <label htmlFor="playlist_description">Description:</label>
                <textarea
                    className="form-control"
                    id="playlist_description"
                    rows={3}
                    value={description}
                    onChange={e => setDescription(e.currentTarget.value)}
                />
            </div>
            <button className="btn btn-danger" onClick={onCancel}>Cancel</button>
            <button className="btn btn-success" onClick={submit}>Save</button>
        </div>
    );
});

// function NameInput() {
//     const [playlistName, setPlaylistName] = useState(playlist.name)

//     return <div className="form-group mb-3">
//         <label htmlFor="playlist_name">Name:</label>
//         <input type="text" className="form-control" id="playlist_name" aria-describedby="helpId" value={playlistName}
//             onChange={event => setPlaylistName(event.currentTarget.value)} />

//         <small id="helpId" className="form-text text-muted">{playlistName.length} / 170</small>
//     </div>
// }
