import React from "react";
import { Playlist } from "../../core/model/Playlist";
import styles from "./PlaylistDetails.module.css";
export interface PlaylistDetailsProps {
  playlist: Playlist;
  /**
   * Edit button clicked
   */
  onEdit?(id: Playlist["id"]): void;
}

export const PlaylistDetails = ({ playlist, onEdit }: PlaylistDetailsProps) => {
  // console.log('Render details');

  return (
    <div data-testid="playlist-details">
      <dl title={playlist.name} data-playlist-id={playlist.id}>
        <dt>Name:</dt>
        <dd style={{ fontSize: "20px" }}>{playlist.name}</dd>

        <dt>Public:</dt>
        <dd
          data-testid="playlist-is-public"
          className={
            styles.playlist_visiblity +
            " " +
            (playlist.public ? styles.playlist_public : styles.playlist_private)
          }
        >
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>

        {playlist.tracks && (
          <>
            <dt>Tracks:</dt>
            <dd>{playlist.tracks?.length}</dd>
          </>
        )}
      </dl>

      {onEdit && (
        <button className="btn btn-info" onClick={() => onEdit(playlist.id)}>
          Edit
        </button>
      )}
    </div>
  );
};
// Are props equal (shouldComponent NOT Update)
// , (prevProps: Readonly<PlaylistDetailsProps>, nextProps: Readonly<PlaylistDetailsProps>) => prevProps.playlist === nextProps.playlist

export default PlaylistDetails;
