import React, { Component, PureComponent } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
    playlists: Playlist[]
    onSelect(id: Playlist['id']): void
}
interface State {

}

export default class PlaylistSelector extends PureComponent<Props, State> {
    state = {}

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="playlist_selector">Select playlist:</label>
                    <select className="form-control" id="playlist_selector"
                        onChange={(event) => {
                            this.props.onSelect(event.currentTarget.selectedOptions[0].value)
                        }}>
                        {this.props.playlists.map(p => <option key={p.id} value={p.id}>{p.name}</option>)}
                    </select>
                </div>
            </div>
        )
    }
}
