import React, { Component, PureComponent } from "react";
import { Track } from "../../core/model/Search";

interface Props {
    track: Track
    onCancel(): void
    onSave(draft: Track): void
}
interface State {
    track: Track
}

// export default class TrackEdit extends Component<Props, State> {
export default class TrackEdit extends PureComponent<Props, State> {
    state = {
        track: this.props.track
    };

    updateTrackName = (name: string) => {
        // this.state.track.name = name // Mutable state // No render with ShouldComponentUpdate() 
        this.setState({
            track: {
                ...this.state.track,
                name
            }
        })
    }

    constructor(props: Props) {
        super(props)
        console.log('constructor')
    }

    componentDidMount() {
        console.log('componentDidMount')
        this.inputRef.current?.focus()
    }

    getSnapshotBeforeUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>) {
        console.log('getSnapshotBeforeUpdate')
        return { mySnapshot: this.inputRef.current?.selectionStart }
    }

    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
        console.log('componentDidUpdate', snapshot)
    }

    componentDidCatch() { console.log('componentDidCatch') }

    // shouldComponentUpdate(nextProps: Readonly<Props>, nextState: Readonly<State>) {
    //     console.log('shouldComponentUpdate')
    //     // return true
    //     return nextProps.track !== this.props.track
    //         || nextState.track !== this.state.track
    //         || nextProps.onSave !== this.props.onSave // PureComponent compares all props!
    // }

    componentWillUnmount() { console.log('componentWillUnmount') }

    static getDerivedStateFromProps(nextProps: Props, nextState: State) {
        console.log('getDerivedStateFromProps')

        return {
            track: nextProps.track.id === nextState.track.id ? nextState.track : nextProps.track
        }
    }

    saveTrack = () => {
        this.props.onSave(this.state.track)
    }

    inputRef = React.createRef<HTMLInputElement>()

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="track_name">Name:</label>
                    <input
                        type="text"
                        ref={this.inputRef}
                        className="form-control"
                        name="track_name"
                        value={this.state.track.name}
                        onChange={e => this.updateTrackName(e.currentTarget.value)}
                        id="track_name"
                        aria-describedby="track_name_helpId"
                        placeholder="Track Name"
                    />
                    {/* <small id="track_name_helpId" className="form-text text-muted">Help text</small> */}
                </div>
                <button className="btn btn-danger" onClick={this.props.onCancel}>Cancel</button>
                <button className="btn btn-success" onClick={this.saveTrack}>Save</button>
            </div>
        );
    }
}
