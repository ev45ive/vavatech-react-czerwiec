import React, { Component, PureComponent } from 'react'
import { Playlist } from '../../core/model/Playlist'
import { PagingObject, Track } from '../../core/model/Search'
import { Time } from './Time'

interface Props {
    tracks: PagingObject<Track>,
    selectedId?: Track['id'],
    onMove(id: Track['id'], insertBefore: number): void
    onRemove(id: Track['id']): void
    onSelect(id: Track['id']): void
}
interface State {

}

export default class TracksList extends PureComponent<Props, State> {
    state = {}

    render() {
        return (
            <div>
                <div className="list-group">
                    {this.props.tracks.items.map((track, index) => <a href="#" key={track.id}
                        className={[
                            "list-group-item list-group-item-action flex-row align-items-start",
                            track.id === this.props.selectedId &&  'active'
                        ].filter(Boolean).join(' ')}
                        onClick={() => this.props.onSelect(track.id)}>
                        <div className="d-flex w-100 justify-content-between">
                            <h5 className="mb-1">{track.name}</h5>
                            <small><Time duration={track.duration_ms} /></small>
                        </div>
                        <p className="mb-1">{track.artists[0].name}</p>
                        <div className="d-flex justify-content-end">
                            <button className="btn btn-sm" onClick={e => {
                                e.stopPropagation()
                                this.props.onMove(track.id, index - 1)
                            }}>
                                <i className="bi-arrow-up-short"></i> Up
                            </button>
                            <button className="btn btn-sm" onClick={e => {
                                e.stopPropagation()
                                this.props.onMove(track.id, index + 1)
                            }}>
                                <i className="bi-arrow-down-short"></i> Down
                            </button>
                            <button className="btn btn-sm" onClick={e => {
                                e.stopPropagation()
                                this.props.onRemove(track.id)
                            }}>&times; Remove</button>
                        </div>
                    </a>)}
                </div>
            </div>
        )
    }
}


