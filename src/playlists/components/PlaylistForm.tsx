import { Playlist } from "../../core/model/Playlist";

import { Control, useForm, useWatch } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
interface Props {
  playlist: Playlist;
  onCancel(): void;
  onSave(draft: Playlist): Promise<string | null>;
}

type PlaylistFormData = Pick<Playlist, "name" | "public" | "description"> & {
  extras: {
    collaborative: boolean;
  };
};

export const InputCounter = ({
  control,
}: {
  control: Control<PlaylistFormData>;
}) => {
  const value = useWatch({ name: "name", control });

  return (
    <small id="helpId" className="form-text text-muted">
      {value?.length} / 170
    </small>
  );
};

const playlistFormSchema = z
  .object({
    name: z.string().min(1, "Field is required").min(3 ),
    description: z.string(),
    public: z.boolean()
  })
  // .refine(async (check) => check.name == check.description, {
  //   message: "custom errror",
  // });

type schemaType = z.infer<typeof playlistFormSchema>

const resolver = zodResolver(playlistFormSchema);

export const PlaylistForm = ({ playlist, onCancel, onSave }: Props) => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    formState: { errors },
  } = useForm<PlaylistFormData>({
    defaultValues: {
      name: "",
      public: false,
      description: "",
    },
    resolver: resolver,
    //   reValidateMode: "onBlur",
    mode: "all",
  });

  // const {name,onBlur,onChange,ref} = register("name", { min: 3, required: true });

  return (
    <form
      data-testid="playlist-form"
      onSubmit={handleSubmit(
        (data) => {
          const { name, public: isPublic, description } = data;
          onSave({
            ...playlist,
            name,
            public: isPublic,
            description,
          });
        },
        (errors) => {
          debugger;
        }
      )}
    >
      <div className="form-group mb-3">
        <label htmlFor="playlist_name">Name:</label>
        <input
          {...register("name", {
            // minLength: {
            //   message: "Minimum length is 3",
            //   value: 3,
            // },
            required: "Name is required",
            shouldUnregister: false,
          })}
          type="text"
          className="form-control"
          id="playlist_name"
          aria-describedby="helpId"
        />
        {errors.name && <p className="text-danger">{errors.name.message}</p>}
        <InputCounter control={control} />
        {/*     
        <small id="helpId" className="form-text text-muted">
          {watch("name").length} / 170
        </small> */}
      </div>

      <div className="form-check mb-3">
        <label className="form-check-label">
          <input
            type="checkbox"
            {...register("public", {})}
            className="form-check-input"
          />{" "}
          Public
        </label>
      </div>

      <div className="form-check mb-3">
        <label className="form-check-label">
          <input
            type="checkbox"
            {...register("extras.collaborative", {})}
            className="form-check-input"
          />{" "}
          Collaborative
        </label>
      </div>

      <div className="form-group mb-3">
        <label htmlFor="playlist_description">Description:</label>
        <textarea
          {...register("description", {})}
          className="form-control"
          id="playlist_description"
          rows={3}
        />
      </div>
      <button className="btn btn-danger" type="button" onClick={onCancel}>
        Cancel
      </button>
      <button className="btn btn-success" type="submit">
        Save
      </button>
    </form>
  );
};
