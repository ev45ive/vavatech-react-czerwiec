import React, { useState } from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
    playlists: Playlist[];
    selectedId?: string;
    onSelected?(id: string): void;
    onDelete?(id: string): void;
}

// Zero One Infinity

export const cls = (...classes: (string | false)[]) =>
    classes.filter(Boolean).join(" ");

export const PlaylistsList = React.memo(({
    playlists,
    selectedId,
    onSelected,
    onDelete,
}: Props) => {
    // console.log('Render PlaylistList');

    return (
        <div>
            <div className="list-group" role="tablist">
                {playlists.map((playlist, index) => (
                    <button
                        data-playlist-id={playlist.id}
                        type="button"

                        role="tab"
                        aria-selected={playlist.id === selectedId}

                        className={cls(
                            "list-group-item list-group-item-action",
                            playlist.id === selectedId && "active"
                        )}
                        key={playlist.id}
                        onClick={() => onSelected && onSelected(playlist.id)}
                    >
                        <span>
                            {index + 1}. {playlist.name}
                        </span>
                        {onDelete && (
                            <span
                            role="button"
                                aria-label="Remove playlist"
                                className="float-end close"
                                onClick={(event) => {
                                    // event.preventDefault()
                                    // event.nativeEvent.stopImmediatePropagation() 
                                    event.stopPropagation()
                                    onDelete(playlist.id)
                                }}
                            >
                                &times;
                            </span>
                        )}
                    </button>
                ))}
            </div>
        </div>
    );
});
//
