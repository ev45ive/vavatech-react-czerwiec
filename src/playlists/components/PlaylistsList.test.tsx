import { fireEvent, getByRole, queryAllByRole, render, screen } from '@testing-library/react'
import React from 'react'
import { Playlist } from '../../core/model/Playlist'
import { mockPlaylists } from '../../stories/mocks/mockPlaylists'
import { PlaylistsList } from './PlaylistsList'

describe('PlaylistList', () => {

    const setup = ({
        selectedId,
        playlists = mockPlaylists
    }: { selectedId?: string, playlists?: Playlist[] }) => {
        const selectSpy = jest.fn()
        const removeSpy = jest.fn()

        render(<PlaylistsList playlists={playlists}
            onSelected={selectSpy}
            onDelete={removeSpy}
            selectedId={selectedId} />)

        return { selectSpy, removeSpy }
    }

    test('renders list with no playlists', () => {
        setup({ playlists: [] })
        expect(screen.queryAllByRole('tab')).toEqual([])
    })

    test('renders list of playlists', () => {
        setup({})
        const listItems = screen.queryAllByRole('tab', {
            name: /Playlist/
        })
        expect(listItems).toHaveLength(3)
        listItems.forEach(item => {
            expect(item).toHaveTextContent('Playlist')
        })
    })

    test('higlight selected playlist', () => {
        setup({ selectedId: '123' })
        const selected = screen.getByRole('tab', {
            selected: true
        })
        expect(selected).toHaveTextContent('Playlist 123')
    })

    test('emit playlist id when selected', () => {
        const { selectSpy } = setup({})
        const selected = screen.getByRole('tab', {
            name: /Playlist 234/
        })
        fireEvent.click(selected)
        expect(selectSpy).toHaveBeenCalledWith('234')
    })

    test('emit playlist id when remove clicked', () => {
        const { removeSpy } = setup({})
        const selected = screen.getByRole('tab', {
            name: /Playlist 234/
        })
        const removeBtn = getByRole(selected, 'button', {
            name: 'Remove playlist'
        })
        fireEvent.click(removeBtn)
        expect(removeSpy).toHaveBeenCalledWith('234')
    })

})
