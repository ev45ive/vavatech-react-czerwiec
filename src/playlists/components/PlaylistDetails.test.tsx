import React from "react";
import { PlaylistDetails } from "./PlaylistDetails";
import { render, screen, fireEvent } from "@testing-library/react";
import renderer from "react-test-renderer";
import { Playlist } from "../../core/model/Playlist";

import styles from "./PlaylistDetails.module.css";
import { Track } from "../../core/model/Search";

describe("PlaylistDetails snapshots", () => {
  it("renders correctly", () => {
    const editSpy = jest.fn();
    const mockPlaylist: Playlist = {
      id: "123",
      name: "Playlist 123",
      public: false,
      description: "playlist 123 is best!",
    };
    const tree = renderer
      .create(<PlaylistDetails playlist={mockPlaylist} onEdit={editSpy} />)
      //   .unmount()
      // .update(<PlaylistDetails playlist={mockPlaylistUpdated} onEdit={editSpy} />)
      .toJSON();

    expect(tree).toMatchSnapshot();
    // › 1 snapshot written.
    // Snapshot Summary
    // › 1 snapshot written from 1 test suite.
  });

  it("renders correctly with tracks", () => {
    const editSpy = jest.fn();
    const mockPlaylist: Playlist = {
      id: "123",
      name: "Playlist 123",
      public: false,
      description: "playlist 123 is best!",
      tracks: [{}, {}, {}] as Track[],
    };
    const tree = renderer
      .create(<PlaylistDetails playlist={mockPlaylist} onEdit={editSpy} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
    // › 1 snapshot written.
    // Snapshot Summary
    // › 1 snapshot written from 1 test suite.
  });
});

describe("PlaylistDetails component", () => {
  test("tests truth", () => {
    expect(true).toBe(true); // Object.is
    expect({ v: [1] }).toEqual({ v: [1] }); // deep equality
  });

  const setup = (playlist?: Partial<Playlist>) => {
    const editSpy = jest.fn();
    const build = (updatePlaylist?: Partial<Playlist>) => {
      const mockPlaylist: Playlist = {
        id: "123",
        name: "Playlist 123",
        public: false,
        description: "playlist 123 is best!",
        ...updatePlaylist,
      };
      return <PlaylistDetails playlist={mockPlaylist} onEdit={editSpy} />;
    };
    const { container, debug, unmount, rerender } = render(build(playlist));

    const updateProps = (playlist?: Partial<Playlist>) => {
      return rerender(build(playlist));
    };

    return {
      unmount,
      editSpy,
      rerender: updateProps,
    };
  };

  test("should emit playlist id when edit button clicked", () => {
    const { editSpy } = setup();

    const editBtn = screen.getByRole("button", {
      name: "Edit",
    });
    // editBtn.click()
    fireEvent(editBtn, new MouseEvent("click", { bubbles: true }));

    expect(editSpy).toHaveBeenCalledWith("123");
  });

  test("renders playlist", () => {
    setup();
    screen.getByText("Playlist 123");
    screen.getByText("No");
    screen.getByText("playlist 123 is best!");
  });

  test("changes color for public playlist", () => {
    const { unmount, rerender } = setup({ public: false });
    const elem = screen.getByTestId("playlist-is-public");
    // expect(elem).toHaveClass('playlist_private')
    expect(elem).toHaveClass(styles.playlist_private);

    // unmount()

    rerender({ public: true });
    const elem2 = screen.getByTestId("playlist-is-public");
    expect(elem2).toHaveClass("playlist_public");
  });
});
