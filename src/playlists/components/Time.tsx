import React, { useEffect, useState } from 'react';

interface TimeProps {
    duration: number;
}

export const Time = React.memo(({ duration }: TimeProps) => {
    const [time, setTime] = useState('00:00:00');

    useEffect(() => {
        const d = new Date(duration);
        setTime(`${d.getUTCHours().toString().padStart(2, '0')
            }:${d.getUTCMinutes().toString().padStart(2, '0')
            }:${d.getUTCSeconds().toString().padStart(2, '0')}`);
    }, [duration]);

    return (
        <span>{time}</span>
    );
})
