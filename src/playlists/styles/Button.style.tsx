
import styled from 'styled-components'

export interface ButtonProps {
    backgroundColor?: string,
    children: React.ReactNode
}

export const Button = styled.button<ButtonProps>`
    color: ${({ theme }) => theme.primary.color};
    padding: 5px;
    background-color: ${({ backgroundColor, theme }) => backgroundColor || 'lightblue'};
    border:1px solid black;
`

export const Button2 = styled('button')<ButtonProps>(({ backgroundColor }) => ({
    color: '#333',
    padding: '5px',
    'background-color': backgroundColor || 'lightblue',
    border: '1px solid black'
}))

export const ButtonsGroup = styled('div')(({ }) => ({
    border: '1px solid black',
    margin: '5px',
    '& button': {
        border: 'none'
    }
}))