
import { Story, Meta } from '@storybook/react';

import { Button, ButtonProps } from './Button.style';

export default {
    title: 'Playlists/Button',
    component: Button,
    argTypes: {
        // backgroundColor: { control: 'color' },
    },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    // children: 'Button',
};