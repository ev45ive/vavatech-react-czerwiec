import {
  act,
  fireEvent,
  getByRole,
  getByText,
  logRoles,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { mockPlaylists } from "../../stories/mocks/mockPlaylists";
import { PlaylistsTDDView } from "./PlaylistsTDDView";
import { mocked } from "ts-jest/utils";
import { Playlist } from "../../core/model/Playlist";
import userEvent from "@testing-library/user-event";

import {
  deletePlaylist,
  fetchPlaylists,
  createPlaylist,
  updatePlaylist,
} from "./PlaylistAPIService";
import { MemoryRouter } from "react-router-dom";

jest.mock("./PlaylistAPIService");

jest.mock("react-router-dom", () => {
  return {
    ...jest.requireActual("react-router-dom"),
    useHistory: () => ({
      push: jest.fn(),
    }),
  };
});

describe("PlaylistsTDDView", () => {
  // setup vs beforeEach:
  // https://kentcdodds.com/blog/avoid-nesting-when-youre-testing
  const setup = ({ playlists = mockPlaylists }) => {
    mocked(fetchPlaylists).mockResolvedValue(playlists);
    mocked(deletePlaylist).mockResolvedValue();

    const { container } = render(<MemoryRouter>
      <PlaylistsTDDView />
    </MemoryRouter>);

    return { container };
  };

  it("shows no playlists", async () => {
    setup({ playlists: [] });

    act(() => {
      /* do something sync - useEffects and useState... */
    });
    // we can check what was rendered

    await act(async () => {
      /* do something Async and wait for useEffects and useState... */
    });
    // we can check what was rendered

    expect(screen.queryAllByRole("tab", { name: /Playlist/ })).toHaveLength(0);
  });

  it("should fetch and show list of playlists", async () => {
    setup({});
    expect(fetchPlaylists).toHaveBeenCalled();
    // screen.getAllByRole('tab', { name: /Playlist/ }) // throws No element found!

    // const items = await waitFor(() => screen.getAllByRole('tab', { name: /Playlist/ }))
    // OR:
    const items = await screen.findAllByRole("tab", { name: /Playlist/ });

    expect(items).toHaveLength(3);
  });

  it("should show details of selected playlist", async () => {
    setup({});
    const item = await screen.findByRole("tab", { name: /Playlist 123/ });

    fireEvent.click(item);

    const details = await screen.findByTestId("playlist-details");
    getByText(details, "Playlist 123");
  });

  it("should remove deleted playlist from list", async () => {
    setup({});
    const item = await screen.findByRole("tab", { name: /Playlist 123/ });
    const removePlaylistBtn = getByRole(item, "button", {
      name: /Remove playlist/,
    });

    mocked(fetchPlaylists).mockResolvedValue(
      mockPlaylists.filter((p) => p.id !== "123")
    );

    fireEvent.click(removePlaylistBtn);

    expect(mocked(deletePlaylist)).toHaveBeenCalledWith<
      Parameters<typeof deletePlaylist>
    >("123");
    await waitForElementToBeRemoved(() =>
      screen.getByRole("tab", { name: /Playlist 123/ })
    );
  });

  it("should close details when selected playlist was deleted", async () => {
    setup({});
    const item = await screen.findByRole("tab", { name: /Playlist 123/ });
    fireEvent.click(item);

    const removePlaylistBtn = getByRole(item, "button", {
      name: /Remove playlist/,
    });

    mocked(fetchPlaylists).mockResolvedValue(
      mockPlaylists.filter((p) => p.id !== "123")
    );

    fireEvent.click(removePlaylistBtn);
    await waitForElementToBeRemoved(() =>
      screen.getByTestId("playlist-details")
    );
  });

  it("should open empty create playlist form", async () => {
    // Arrange ( Given ...)
    const { container } = setup({});
    // logRoles(document.body)
    // logRoles(container)

    // Act ( When ...)
    const btn = screen.getByRole("button", { name: /Create New/ });
    fireEvent.click(btn);

    // Assert ( Then ...)
    await screen.findByTestId("playlist-form");
  });

  it("should show created playlist on the list", async () => {
    setup({});
    // Red => Green => Refactor

    const btn = await screen.findByRole("button", { name: /Create New/ });
    fireEvent.click(btn /* new MouseEvent('click', {altKey:true}) */);

    // https://testing-library.com/docs/ecosystem-user-event/
    const nameInput = screen.getByLabelText("Name:");
    userEvent.type(nameInput, "Playlist ABC", {});

    const publicCheckbox = screen.getByLabelText("Public");
    userEvent.click(publicCheckbox /*, {altKey:true} */);

    const descrTextArea = screen.getByLabelText("Description:");
    userEvent.type(descrTextArea, "Ala ma kota", {});

    // const fakeName  = 'Playlist ABC'
    // const fakeName  = faker.people.name({...}) // test with random seed!

    mocked(createPlaylist).mockResolvedValue({
      id: "abc",
      name: "Playlist ABC",
      public: true,
      description: "Ala ma kota",
    });

    await act(async () => {
      userEvent.click(screen.getByRole("button", { name: "Save" }));
    });

    expect(mocked(createPlaylist)).toHaveBeenCalledWith(
      expect.objectContaining({
        name: "Playlist ABC",
        public: true,
        // description: expect.any(String)
        description: "Ala ma kota",
      })
    );

    const item = await screen.findByRole("tab", { name: /Playlist ABC/ }, {});
  });

  it("should open edit playlist form and close details", async () => {
    setup({});

    // select playlist
    const item = await screen.findByRole("tab", { name: /Playlist 123/ });
    fireEvent.click(item);

    // edit playlist
    userEvent.click(await screen.findByRole("button", { name: "Edit" }));

    // hide details
    // await waitForElementToBeRemoved(() => screen.getByTestId('playlist-details'))
    expect(screen.queryByTestId("playlist-details")).not.toBeInTheDocument();

    // show form with playlist data
    await screen.findByTestId("playlist-form");
    const nameInput = screen.getByLabelText("Name:");
    // screen.debug(await screen.findByTestId('playlist-form'))

    expect(nameInput).toHaveValue("Playlist 123");
  });

  it.only("should show updated playlist on the list", async () => {
    setup({});
    const item = await screen.findByRole("tab", { name: /Playlist 123/ });

    fireEvent.click(item);
    userEvent.click(await screen.findByRole("button", { name: "Edit" }));

    const nameInput = screen.getByLabelText("Name:");
    userEvent.clear(nameInput);
    userEvent.type(nameInput, "Playlist ABC", {});

    const publicCheckbox = screen.getByLabelText("Public");
    userEvent.click(publicCheckbox /*, {altKey:true} */);

    const descrTextArea = screen.getByLabelText("Description:");
    userEvent.clear(descrTextArea);
    userEvent.type(descrTextArea, "Ala ma kota", {});

    mocked(updatePlaylist).mockResolvedValue();
    mocked(fetchPlaylists).mockResolvedValue(
      mockPlaylists.map((p) =>
        p.name.includes("Playlist 123")
          ? {
              id: "123",
              name: "Playlist ABC",
              public: true,
              // description: expect.any(String)
              description: "Ala ma kota",
            }
          : p
      )
    );

    await act(async () => {
      userEvent.click(screen.getByRole("button", { name: "Save" }));
    });

    expect(mocked(updatePlaylist)).toHaveBeenCalledWith(
      expect.objectContaining({
        name: "Playlist ABC",
        public: true,
        // description: expect.any(String)
        description: "Ala ma kota",
      })
    );

    const saveditem = await screen.findByRole(
      "tab",
      { name: /Playlist ABC/ },
      {}
    );
  });
});
