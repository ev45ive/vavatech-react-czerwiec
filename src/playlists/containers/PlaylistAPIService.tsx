import axios from "axios";
import { Playlist } from "../../core/model/Playlist";
import { PagingObject } from "../../core/model/Search";
import { UserProfile } from "../../core/model/UserProfile";

const PlaylistAPIService = {};

export async function fetchPlaylists(): Promise<Playlist[]> {
  const { data } = await axios.get<PagingObject<Playlist>>(
    "https://api.spotify.com/v1/me/playlists"
  );
  return data.items;
}

export async function fetchPlaylist(id: Playlist["id"]): Promise<Playlist> {
  const { data } = await axios.get<Playlist>(
    "https://api.spotify.com/v1/playlists/" + id
  );
  return data;
}

export function deletePlaylist(id: Playlist["id"]): Promise<void> {
  throw new Error("Function not implemented.");
}

export async function createPlaylist(
  user_id: UserProfile["id"],
  draft: Pick<Playlist, "name" | "description" | "public">
): Promise<Playlist> {
  const { data } = await axios.post<Playlist>(
    `https://api.spotify.com/v1/users/${user_id}/playlists`,
    {
      name: draft.name,
      description: draft.description || "",
      public: draft.public,
    }
  );
  return data;
}

export function updatePlaylist(
  draft: Pick<Playlist, "id" | "name" | "description" | "public">
): Promise<void> {
  throw new Error("Function not implemented.");
}
