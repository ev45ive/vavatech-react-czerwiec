import React, { Component } from 'react'
import { Playlist } from '../../core/model/Playlist'
import { PagingObject, Track } from '../../core/model/Search'
import { mockPlaylists } from '../../stories/mocks/mockPlaylists'
import { mockTracksPagingObject, mockTracks2 } from '../../stories/mocks/mockTracks'
import PlaylistDetails from '../components/PlaylistDetails'
import PlaylistSelector from '../components/PlaylistSelector'
import TrackEdit from '../components/TrackEdit'
import TracksList from '../components/TracksList'

interface Props {

}
interface State {
    playlists: Playlist[]
    selectedPlaylist?: Playlist
    tracks?: PagingObject<Track>
    selectedTrack?: Track
}

export default class PlaylistTracksView extends Component<Props, State> {
    state: State = {
        playlists: mockPlaylists,
        // selectedPlaylist: mockPlaylists[0],
        // tracks: mockTracksPagingObject as PagingObject<Track>
    }

    selectPlaylist = (id: Playlist['id']) => {
        this.setState({
            selectedPlaylist: this.state.playlists.find(p => p.id === id),
            tracks: { items: mockTracks2 } as PagingObject<Track>
        })
    }

    updateTrack = (draft: Track) => {
        this.setState(state => {
            if (!state.tracks) { return {} }

            return {
                tracks: {
                    ...state.tracks,
                    items: state.tracks.items.map(t => t.id === draft.id ? draft : t)
                }
            }
        })
    }

    selectTrack = (id?: Track['id']) => {
        this.setState({
            selectedTrack: this.state.tracks?.items.find(t => t.id === id)
        })
    }

    moveTrack = (id: Track['id'], insertBefore: number) => {
        this.setState(state => {
            if (!state.tracks) { return {} }

            const index = state.tracks.items.findIndex(t => t.id === id)
            if (index !== -1 && insertBefore >= 0) {
                const item = state.tracks.items.splice(index, 1)
                state.tracks.items.splice(insertBefore, 0, ...item)
            }

            return {
                tracks: {
                    ...state.tracks,
                    items: state.tracks.items
                }
            }
        })
    }

    removeTrack = (id: Track['id']) => {
        this.setState(state => {
            return state.tracks ? {
                tracks: {
                    ...state.tracks,
                    items: state.tracks.items.filter(t => t.id !== id),
                }
            } : {}
        })
        this.setState(state => ({
            selectedTrack: state.tracks?.items.find(t => t.id === state.selectedTrack?.id)
        }))
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col">
                        <PlaylistSelector
                            playlists={this.state.playlists}
                            onSelect={this.selectPlaylist} />

                        {this.state.selectedPlaylist &&
                            <div className="card mt-4">
                                <div className="card-body">
                                    <PlaylistDetails
                                        playlist={this.state.selectedPlaylist} />
                                </div>
                            </div>
                        }
                    </div>
                    <div className="col">
                        {this.state.tracks && <TracksList
                            selectedId={this.state.selectedTrack?.id}
                            tracks={this.state.tracks}
                            onSelect={this.selectTrack}
                            onMove={this.moveTrack}
                            onRemove={this.removeTrack} />}

                        <div className="mb-3"></div>

                        {/* {this.state.selectedTrack && <TrackEdit
                            track={this.state.selectedTrack}
                            onCancel={() => this.selectTrack()} // <- Breaks PureComponent 
                            onSave={this.updateTrack} />} */}


                        {this.state.selectedTrack && <TrackEdit
                            track={this.state.selectedTrack}
                            onCancel={this.selectTrack}
                            onSave={this.updateTrack} />}
                    </div>
                </div>
            </div>
        )
    }
}

// export const PlaylistTracksView = (props: Props) => {
//    const [stateA, setStateA] = useState('A')
//    const [stateB, setStateB] = useState('B')
//     return ( <div>PlaylistTracksView</div> )
// }
