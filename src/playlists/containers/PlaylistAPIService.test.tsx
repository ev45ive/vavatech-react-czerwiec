import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { Playlist } from '../../core/model/Playlist'
import { PagingObject } from '../../core/model/Search'
import { mockPlaylists } from '../../stories/mocks/mockPlaylists'
import { fetchPlaylists } from './PlaylistAPIService'

const handlers = [
    rest.get('https://api.spotify.com/v1/me/playlists', (req, res, ctx) => {
        return res(
            ctx.status(200),
            ctx.json({
                items: mockPlaylists
            } as PagingObject<Playlist>),
        )
    })
]

const server = setupServer(...handlers)

describe('Playlist API Service', () => {

    beforeAll(() => server.listen())
    afterEach(() => server.resetHandlers())
    afterAll(() => server.close())

    beforeEach(() => {
    })

    test('should fetch Playlists', async () => {
        const result = await fetchPlaylists();
        expect(result).toEqual(mockPlaylists)
    })

    test.todo('should delete Playlist')

    test.todo('should create Playlist')

    test.todo('should update Playlist')

})
