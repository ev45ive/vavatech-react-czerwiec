// tsrafc
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistEditForm } from "../components/PlaylistEditForm";
import { PlaylistsList } from "../components/PlaylistsList";
import { mockPlaylists } from "../../stories/mocks/mockPlaylists";
import { SearchForm } from "../../music-search/components/SearchForm";

interface Props { }

export const PlaylistsView = (props: Props) => {
    const [filter, setFilter] = useState('')
    const [playlists, setPlaylists] = useState(mockPlaylists)
    const [selectedId, setSelectedId] = useState<Playlist["id"] | undefined>();
    const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist | undefined>();
    const [mode, setMode] = useState<"details" | "edit" | "create">("details");

    useEffect(() => {
        if (selectedId)
            setSelectedPlaylist(playlists.find((p) => p.id === selectedId));
    }, [selectedId, playlists]);

    const switchToEdit = useCallback(() => { setMode("edit"); }, [])
    const switchToDetails = useCallback(() => { setMode("details"); }, [])
    const switchToCreate = useCallback(() => { setMode("create"); }, [])

    const addPlaylist = useCallback(async (draft: Playlist) => {
        if (draft.name === '') {
            return 'Missing playlist name'
        }
        draft.id = Date.now() + Math.ceil(Math.random() * 10_000).toString()
        setPlaylists(playlists => [...playlists, draft])

        setSelectedId(draft.id)
        setMode("details");
        return null
    }, [])

    // const data = useCallback(() => window.impureData, [window.impureData])
    // const data = useCallback(makeDataSelector(paramA), [paramA])

    const deletePlaylist = useCallback((id: Playlist['id']) => {
        setPlaylists(playlists => playlists.filter(p => p.id !== id))
    }, [])

    const savePlaylist = useCallback(async (draft: Playlist) => {
        if (draft.name === '') {
            return 'Missing playlist name'
        }

        setPlaylists(playlists => playlists.map(p => p.id === draft.id ? draft : p))
        setMode("details");
        return null
    }, [])

    const emptyPlaylist = useMemo(() => ({
        id: '',
        description: '',
        name: '',
        public: false
    }), [])

    return <div>
        <div className="row">
            <div className="col">
                <SearchForm query={filter} onSearch={setFilter} />

                <div className="mb-3"></div>

                <PlaylistsList
                    playlists={playlists}
                    selectedId={selectedId}
                    onSelected={setSelectedId}
                    onDelete={deletePlaylist}
                />

                <button className="btn btn-info float-end mt-3" onClick={switchToCreate}>
                    Create New
                </button>
            </div>
            <div className="col">
                {selectedPlaylist && mode === "details" && (
                    <PlaylistDetails
                        playlist={selectedPlaylist}
                        onEdit={switchToEdit}
                    />
                )}

                {selectedPlaylist && mode === "edit" && (
                    <PlaylistEditForm
                        playlist={selectedPlaylist}
                        onCancel={switchToDetails}
                        onSave={savePlaylist}
                    />
                )}
                {mode === "create" && (
                    <PlaylistEditForm
                        playlist={emptyPlaylist}
                        onCancel={switchToDetails}
                        onSave={addPlaylist}
                    />
                )}
            </div>

        </div>
    </div>
};
