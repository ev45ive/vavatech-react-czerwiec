import {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { Route, useHistory } from "react-router-dom";
import { Playlist } from "../../core/model/Playlist";
import { UserContext } from "../../core/providers/UserProvider";
import PlaylistDetails from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistsList } from "../components/PlaylistsList";
import {
  fetchPlaylists,
  deletePlaylist,
  createPlaylist,
  updatePlaylist,
} from "./PlaylistAPIService";


type playlistDetailsUrl = `/playlists/${keyof Playlist}/details`;
const url:playlistDetailsUrl = '/playlists/name/details'

interface Props {}

export const PlaylistsTDDView = (props: Props) => {
  const [playlists, setPlaylists] = useState<Playlist[]>([]);
  const [selectedId, setSelectedId] = useState<string | undefined>();
  const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist>();
  const [mode, setMode] = useState<"create" | "details" | "edit">("details");

  const [message, setMessage] = useState("");

  const { user } = useContext(UserContext);

  const { push } = useHistory();

  useEffect(() => {
    fetchPlaylists()
      .then(setPlaylists)
      .catch((error) => setMessage(error.message));
  }, []);

  useEffect(() => {
    setSelectedPlaylist(playlists.find((p) => p.id === selectedId));
  }, [selectedId, playlists]);

  const removePlaylist = async (id: Playlist["id"]) => {
    await deletePlaylist(id);
    setPlaylists(await fetchPlaylists());
  };

  // const createPlaylistMode = useCallback(() => setMode('create'), [])
  const createPlaylistMode = useCallback(() => push("/playlists/create"), []);

  const emptyPlaylist: Playlist = useMemo(
    () => ({ id: "", name: "", public: false, description: "" }),
    []
  );

  const addNewPlaylist = async (draft: Playlist) => {
    try {
      if (!user) {
        throw new Error("Not logged in");
      }

      const saved = await createPlaylist(user.id, draft);
      setPlaylists([...playlists, saved]);
      return null;
    } catch (error) {
      setMessage(error.message);
      return "";
    }
  };

  const savePlaylist = async (draft: Playlist) => {
    await updatePlaylist(draft);
    setPlaylists(await fetchPlaylists());
    return null;
  };

  const editPlaylistMode = (selectedId: Playlist["id"]) => {
    push("/playlists/edit/" + selectedId);
  };

  return (
    <div>
      {message && <p className="alert alert-danger mb-3">{message}</p>}

      <div className="row">
        <div className="col">
          <PlaylistsList
            playlists={playlists}
            selectedId={selectedId}
            onSelected={(selectedId) => {
              push("/playlists/details/" + selectedId);
            }}
            onDelete={removePlaylist}
          />
          <div className="mb-3"></div>

          <button className="btn btn-info" onClick={createPlaylistMode}>
            Create New Playlist
          </button>
        </div>
        <div className="col">
          <Route
            path="/playlists/details/:playlist_id"
            render={({ match }) => {
              setSelectedId(match.params.playlist_id);
              return (
                selectedPlaylist && (
                  <PlaylistDetails
                    playlist={selectedPlaylist}
                    onEdit={editPlaylistMode}
                  />
                )
              );
            }}
          />

          <Route
            path="/playlists/edit/:playlist_id"
            render={({ match }) => {
              setSelectedId(match.params.playlist_id);

              return (
                selectedPlaylist && (
                  <PlaylistForm
                    playlist={selectedPlaylist}
                    onCancel={() => {}}
                    onSave={savePlaylist}
                  />
                )
              );
            }}
          />

          <Route
            path="/playlists/create"
            render={() => (
              <PlaylistForm
                playlist={emptyPlaylist}
                onCancel={() => {}}
                onSave={addNewPlaylist}
              />
            )}
          />
        </div>
      </div>
    </div>
  );
};
