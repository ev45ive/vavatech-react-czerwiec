import { useCallback, useContext, useEffect, useMemo } from "react";
import { useDispatch } from "react-redux";
import { Route, useHistory } from "react-router-dom";
import { Playlist } from "../../core/model/Playlist";
import { UserContext } from "../../core/providers/UserProvider";
import * as fromReducer from "../../core/reducers/PlaylistsReducer";
import { useAppSelector } from "../../store";
import PlaylistDetails from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistsList } from "../components/PlaylistsList";

type playlistDetailsUrl = `/playlists/${keyof Playlist}/details`;
const url: playlistDetailsUrl = "/playlists/name/details";

interface Props {}

export const PlaylistsReduxView = (props: Props) => {
  const { user } = useContext(UserContext);
  const { push } = useHistory();

  const dispatch = useDispatch();
  const playlists = useAppSelector(fromReducer.selectPlaylists);
  const selectedPlaylist = useAppSelector(fromReducer.selectPlaylist);
  const { error: message, loading } = useAppSelector(
    fromReducer.selectPlaylistsMeta
  );

  useEffect(() => {
    dispatch(fromReducer.loadPlaylists());
  }, []);

  const removePlaylist = async (id: Playlist["id"]) => {
    dispatch(fromReducer.removePlaylists({ id }));
  };

  const createPlaylistMode = useCallback(() => push("/playlists/create"), []);

  const emptyPlaylist: Playlist = useMemo(
    () => ({ id: "", name: "", public: false, description: "" }),
    []
  );

  const addNewPlaylist = async (draft: Playlist) => {
    if (!user) {
      return null;
    }
    dispatch(
      fromReducer.addPlaylist({
        user_id: user.id,
        draft,
      })
    );
    return null;
  };

  const savePlaylist = async (draft: Playlist) => {
    dispatch(fromReducer.savePlaylist(draft));
    return null;
  };

  const editPlaylistMode = (selectedId: Playlist["id"]) => {
    push("/playlists/edit/" + selectedId);
  };

  return (
    <div>
      {message && <p className="alert alert-danger mb-3">{message}</p>}

      <div className="row">
        <div className="col">
          <PlaylistsList
            playlists={playlists}
            selectedId={selectedPlaylist?.id}
            onSelected={(selectedId) => {
              push("/playlists/details/" + selectedId);
            }}
            onDelete={removePlaylist}
          />
          <div className="mb-3"></div>

          <button className="btn btn-info" onClick={createPlaylistMode}>
            Create New Playlist
          </button>
        </div>
        <div className="col">
          <Route
            path="/playlists/details/:playlist_id"
            render={({ match }) => {
              dispatch(
                fromReducer.selectPlaylistById({ id: match.params.playlist_id })
              );
              return (
                selectedPlaylist && (
                  <PlaylistDetails
                    playlist={selectedPlaylist}
                    onEdit={editPlaylistMode}
                  />
                )
              );
            }}
          />

          <Route
            path="/playlists/edit/:playlist_id"
            render={({ match }) => {
              dispatch(
                fromReducer.selectPlaylistById({ id: match.params.playlist_id })
              );

              return (
                selectedPlaylist && (
                  <PlaylistForm
                    playlist={selectedPlaylist}
                    onCancel={() => {}}
                    onSave={savePlaylist}
                  />
                )
              );
            }}
          />

          <Route
            path="/playlists/create"
            render={() => (
              <PlaylistForm
                playlist={emptyPlaylist}
                onCancel={() => {}}
                onSave={addNewPlaylist}
              />
            )}
          />
        </div>
      </div>
    </div>
  );
};
