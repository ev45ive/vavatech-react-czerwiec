export const mockAlbums = [
    { id: '123', name: 'Album 123', images: [{ url: 'https://www.placecage.com/c/300/300' }] },
    { id: '234', name: 'Album 234', images: [{ url: 'https://www.placecage.com/c/400/400' }] },
    { id: '345', name: 'Album 345', images: [{ url: 'https://www.placecage.com/c/500/500' }] },
    { id: '456', name: 'Album 456', images: [{ url: 'https://www.placecage.com/c/600/600' }] },
];
