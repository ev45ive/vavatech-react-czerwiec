import { Playlist } from "../../core/model/Playlist";

export const mockPlaylists: Playlist[] = [
    {
        id: "123",
        name: "Playlist 123",
        public: false,
        description: "playlist 123 is best!",
    },
    {
        id: "234",
        name: "Playlist 234",
        public: true,
        description: "playlist 234 is best!",
    },
    {
        id: "345",
        name: "Playlist 345",
        public: false,
        description: "playlist 345 is best!",
    },
];
