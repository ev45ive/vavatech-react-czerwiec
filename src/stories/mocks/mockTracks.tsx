import { PagingObject, Track } from "../../core/model/Search";

type MockTrack = Pick<Track, 'id' | 'name' | 'duration_ms'> & { artists: Pick<Track['artists'][0], 'name'>[] }

// https://dev.to/iainfreestone/20-resources-for-generating-fake-and-mock-data-55g1
// https://github.com/marak/Faker.js/
// https://chancejs.com/miscellaneous/guid.html
// https://github.com/boo1ean/casual 
// https://github.com/keikaavousi/fake-store-api 

// https://miragejs.com/
// https://storybook.js.org/addons/msw-storybook-addon 
// https://msw-sb.netlify.app/?path=/story/guides-introduction--page 
// https://jsonplaceholder.typicode.com/ 
// https://github.com/typicode/json-server

export const mockTracks: MockTrack[] = [
    { id: 'A123', name: 'Track A123', artists: [{ name: 'Artist A123' }], duration_ms: 3.5 * 60 * 1000 },
    { id: 'A234', name: 'Track A234', artists: [{ name: 'Artist A234' }], duration_ms: 2.5 * 60 * 1000 },
    { id: 'A345', name: 'Track A345', artists: [{ name: 'Artist A345' }], duration_ms: 1.5 * 60 * 1000 },
    { id: 'A456', name: 'Track A456', artists: [{ name: 'Artist A456' }], duration_ms: 1.5 * 60 * 1000 },
]

export const mockTracks2: MockTrack[] = [
    { id: 'B123', name: 'Track B123', artists: [{ name: 'Artist B123' }], duration_ms: 3.5 * 60 * 1000 },
    { id: 'B234', name: 'Track B234', artists: [{ name: 'Artist B234' }], duration_ms: 2.5 * 60 * 1000 },
    { id: 'B345', name: 'Track B345', artists: [{ name: 'Artist B345' }], duration_ms: 1.5 * 60 * 1000 },
    { id: 'B456', name: 'Track B456', artists: [{ name: 'Artist B456' }], duration_ms: 1.5 * 60 * 1000 },
]

export const mockTracksPagingObject: PagingObject<MockTrack> = {
    items: mockTracks,
    href: '',
    limit: 20,
    next: '',
    offset: 20,
    previous: '',
    total: 100
}