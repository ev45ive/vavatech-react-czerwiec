import { Meta, Story } from "@storybook/react";
import { MusicSearchView } from "../../music-search/containers/MusicSearchView";
import { rest } from 'msw'
import { mockAlbums } from "../mocks/mockAlbums";

export default {
    title: 'MusicSearch/MusicSearchView',
    component: MusicSearchView,
} as Meta;

const Template: Story<{}> = (args) => <MusicSearchView {...args} />;

export const Primary = Template.bind({});
Primary.args = {

};
Primary.parameters = {
    msw: [
        rest.get('https://api.spotify.com/v1/search', (req, res, ctx) => {
            // debugger
            return res(
                ctx.json(mockAlbums),
            );
        }),
    ]
}