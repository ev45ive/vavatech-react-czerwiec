import { Meta, Story } from "@storybook/react";
import { Album } from "../../core/model/Search";
import { AlbumCard, AlbumCardProps } from "../../music-search/components/AlbumCard";

export default {
    title: 'MusicSearch/AlbumCard',
    component: AlbumCard,
    decorators:[
        Story => <div style={{maxWidth:'500px'}}>
            <Story/>
        </div>
    ]
} as Meta;

const Template: Story<AlbumCardProps> = (args) => <AlbumCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    album: {
        id: '123',
        name: 'Test album',
        images: [
            { url: 'https://www.placecage.com/c/300/300' }
        ] //as unknown as Album['images']
    }// as unknown as Album
};

export const NoImage = Template.bind({});
NoImage.args = {
    album: {
        id: '123',
        name: 'Test album',
        images: []
    } //as unknown as Album
};