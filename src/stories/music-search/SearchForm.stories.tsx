import { Meta, Story } from "@storybook/react";
import { SearchForm, SearchFormProps } from "../../music-search/components/SearchForm";
import { MusicSearchView } from "../../music-search/containers/MusicSearchView";

export default {
    title: 'MusicSearch/SearchForm',
    component: SearchForm,
} as Meta;

const Template: Story<SearchFormProps> = (args) => <SearchForm {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    query:'test'
};