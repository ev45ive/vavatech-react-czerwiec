import { Meta, Story } from "@storybook/react";
import { Album } from "../../core/model/Search";
import { SearchForm, SearchFormProps } from "../../music-search/components/SearchForm";
import { SearchResults, SearchResultsProps } from "../../music-search/components/SearchResults";
import { MusicSearchView } from "../../music-search/containers/MusicSearchView";
import { mockAlbums } from "../mocks/mockAlbums";

export default {
    title: 'MusicSearch/SearchResults',
    component: SearchResults,
} as Meta;

const Template: Story<SearchResultsProps> = (args) => <SearchResults {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    results: mockAlbums as Album[]
};