import { Meta, Story } from "@storybook/react";
import PlaylistDetails, { PlaylistDetailsProps } from "../../playlists/components/PlaylistDetails";


export default {
    title: 'Playlists/Details',
    component: PlaylistDetails,
    // argTypes: {
    //     backgroundColor: { control: 'color' },
    // },
    // decorators: [
    //     (Story) => (
    //         <div style={{ margin: '3em' }}>
    //             <Story />
    //         </div>
    //     ),
    // ],
} as Meta;

const Template: Story<PlaylistDetailsProps> = (args) => <PlaylistDetails {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    playlist: {
        id: '123', name: '123', description: '', public: false,
    }
};