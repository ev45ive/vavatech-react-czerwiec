import React, { useEffect, useRef, useState } from "react";

export interface SearchFormProps {
  query: string;
  onSearch(query: string): void;
}

export const SearchForm = ({
  query: parentQuery,
  onSearch,
}: SearchFormProps) => {
  const [query, setQuery] = useState(parentQuery);

  useEffect(() => setQuery(parentQuery), [parentQuery]);

  useEffect(() => {
    if (!query) return;

    const handle = setTimeout(() => {
      onSearch(query);
    }, 500);

    return () => clearTimeout(handle);
  }, [query]);

  return (
    <div>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <button
          className="btn btn-outline-secondary"
          type="submit"
          onClick={() => onSearch(query)}
        >
          Search
        </button>
      </div>
    </div>
  );
};
