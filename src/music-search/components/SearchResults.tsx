import React from 'react'
import { Album } from '../../core/model/Search'
import { AlbumCard } from './AlbumCard'

export  interface SearchResultsProps {
    results: Album[]
}

export const SearchResults = ({ results }: SearchResultsProps) => {
    return (
        <div>
            <div className="row row-cols-1 row-cols-md-4 g-0">
                {results.map(result => <div className="col" key={result.id}>
                    <AlbumCard album={result} />
                </div>)}
            </div>
        </div>
    )
}
