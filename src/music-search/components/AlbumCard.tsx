import React from 'react'
import { Album, SimpleAlbum } from '../../core/model/Search'

export interface AlbumCardProps {
    album: SimpleAlbum
}

export const AlbumCard = ({ album }: AlbumCardProps) => {
    return (
        <div className="card">
            <img src={album.images[0]?.url || 'https://www.placecage.com/300/300'} className="card-img-top" />
            <div className="card-body">
                <h5 className="card-title">
                    {album.name}
                </h5>
            </div>
        </div>
    )
}
