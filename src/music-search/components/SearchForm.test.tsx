import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { SearchForm } from "./SearchForm";

describe.only("SearchForm", () => {
  const setup = ({ query = "" }) => {
    const onSearchSpy = jest.fn();

    const build = ({ query = "" }) => (
      <SearchForm onSearch={onSearchSpy} query={query} />
    );

    const { rerender, unmount } = render(build({ query }));

    const update = ({ query = "" }) => rerender(build({ query }));

    return { update, unmount, onSearchSpy };
  };

  test.todo("show input with initial query");

  test.todo("updates input when parent query changes");

  test.todo("emits query when search button is clicked");

  test("emits query when user stops typing", async () => {
    jest.useFakeTimers();

    const { onSearchSpy } = setup({ query: "" });

    userEvent.type(await screen.getByPlaceholderText("Search"), "batman", {});

    jest.advanceTimersByTime(499);
    expect(onSearchSpy).not.toHaveBeenCalledWith("batman");

    jest.advanceTimersByTime(499);
    expect(onSearchSpy).toHaveBeenCalledWith("batman");
    expect(onSearchSpy).toHaveBeenCalledTimes(1);
  });
});

// export const delay = (ms = 100) => {
//   return new Promise((resolve) => setTimeout(resolve, ms));
// };
