import { useEffect, useReducer } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { SearchForm } from "../components/SearchForm";
import { SearchResults } from "../components/SearchResults";
import * as fromReducer from "../../core/reducers/MusicSearchReducer";

export const MusicSearchView = (props: {}) => {
  const [{ loading, message, query, results }, dispatch] = useReducer(
    fromReducer.reducer,
    fromReducer.initialState
  );
  const { push } = useHistory();
  const { search } = useLocation();

  useEffect(() => {
    const q = new URLSearchParams(search).get("q");
    fromReducer.fetchAlbums(q)(dispatch);
  }, [search]);

  const searchAlbums = (query: string): void => {
    push("/search?q=" + query);
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm query={query} onSearch={searchAlbums} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {message && <p className="alert alert-danger">{message}</p>}
          {loading && <Spinner/>}

          <SearchResults results={results} />
        </div>
      </div>
    </div>
  );
};

export const Spinner = () => (
  <div className="d-flex justify-content-center">
    <div className="spinner-border" role="status">
      <span className="visually-hidden">Loading...</span>
    </div>
  </div>
);
