import { useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { SearchForm } from "../components/SearchForm";
import { SearchResults } from "../components/SearchResults";
import { useDispatch, useSelector } from "react-redux";
import * as fromReducer from "../../core/reducers/MusicSearchReducer";

export const MusicSearchReduxView = (props: {}) => {
  const { push } = useHistory();
  const { search } = useLocation();

  const dispatch = useDispatch();
  const { loading, message, query, results } = useSelector(
    fromReducer.selectSearchFeature
  );

  useEffect(() => {
    const q = new URLSearchParams(search).get("q");
    // dispatch(fromReducer.searchStart({query})) // Sync
    // fromReducer.fetchAlbums(dispatch, q); // Inverted ASync
    dispatch(fromReducer.fetchAlbums(q)); // Middleware Thunk
  }, [search]);

  const searchAlbums = (query: string): void => {
    push("/search?q=" + query);
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm query={query} onSearch={searchAlbums} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {message && <p className="alert alert-danger">{message}</p>}
          {loading && <Spinner />}

          <SearchResults results={results} />
        </div>
      </div>
    </div>
  );
};

export const Spinner = () => (
  <div className="d-flex justify-content-center">
    <div className="spinner-border" role="status">
      <span className="visually-hidden">Loading...</span>
    </div>
  </div>
);
