
import { createAsyncThunk, createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit"
import { fetchPlaylists, fetchPlaylist, createPlaylist } from "../../playlists/containers/PlaylistAPIService"
import { AppDispatch, AppState } from "../../store"
import { Playlist } from "../model/Playlist"


interface PlaylistsState {
    playlists: Playlist[]
    playlist?: Playlist
    selectedId?: Playlist['id']
    meta: {
        loading: boolean
        error: string
    }
}

const initialState: PlaylistsState = {
    playlists: [],
    meta: {
        error: '',
        loading: false
    }
}

export const loadPlaylist = createAsyncThunk<Playlist, { id: Playlist['id'] }>(
    'playlists/fetchPlaylist',
    async ({ id }, thunkAPI) => {
        const playlist = selectPlaylist(thunkAPI.getState() as AppState)
        if (playlist?.id == id) {
            return playlist
        }
        return await fetchPlaylist(id)
    }
)

export const addPlaylist = createAsyncThunk<Playlist, {
    user_id: string, draft: Pick<Playlist, "name" | "description" | "public">
}>(
    'playlists/addPlaylist',
    async ({ user_id, draft }, thunkAPI) => {
        // Add playlist
        const resp = await createPlaylist(user_id, draft)
        // refresh playlists
        thunkAPI.dispatch(loadPlaylists())
        return resp
    }
)



export const featureKey = 'playlists'

export const playlistsSlice = createSlice({
    name: featureKey,
    initialState,
    reducers: {
        fetchPlaylistsStart(state, action) {
            state.meta.loading = true
        },
        fetchPlaylistsSucces(state, action: PayloadAction<{ data: Playlist[] }>) {
            state.meta.loading = false
            state.playlists = action.payload.data
        },
        fetchPlaylistsFailed(state, action: PayloadAction<{ error: Error }>) {
            state.meta.loading = true
            state.meta.error = action.payload.error.message
        },
        selectPlaylistById(state, action: PayloadAction<{ id: string }>) {
            state.selectedId = action.payload.id
        }
    },
    extraReducers: (builder) => {
        // Add reducers for additional action types here, and handle loading state as needed
        builder.addCase(loadPlaylist.fulfilled, (state, action) => {
            state.playlist = action.payload
            state.meta.loading = false
        })
        builder.addCase(addPlaylist.fulfilled, (state, action) => {
            state.playlist = action.payload
            state.meta.loading = false
        })
        builder.addMatcher(action => action.type.includes('rejected'), (state, action) => {
            state.meta.loading = false
            state.meta.error = action.payload.error.message
        })
        builder.addMatcher(action => action.type.includes('pending'), (state, action) => {
            state.meta.loading = true
            state.meta.error = ''
        })
    },
})

export const {
    fetchPlaylistsFailed,
    fetchPlaylistsStart,
    fetchPlaylistsSucces,
    selectPlaylistById
} = playlistsSlice.actions

export function loadPlaylists() {
    return (dispatch: Dispatch) => {
        dispatch(fetchPlaylistsStart({}))
        fetchPlaylists()
            .then(data => dispatch(fetchPlaylistsSucces({ data })))
            .catch((error) => dispatch(fetchPlaylistsFailed({ error })))
    }
}


/* Selectors */
export const selectPlaylistsState = (state: AppState) => state[featureKey]

export const selectPlaylistsMeta = (state: AppState) => selectPlaylistsState(state).meta

export const selectPlaylists = (state: AppState) => selectPlaylistsState(state).playlists

export const selectPlaylist = (state: AppState) => {
    const { playlists, selectedId } = selectPlaylistsState(state)
    return playlists.find(p => p.id === selectedId)
}
// export const selectPlaylist = (state: AppState) => {
//     return selectPlaylistsState(state).playlist
// }



export default playlistsSlice.reducer



// export function fetchPlaylist(arg0: { id: string }): any {
//     return (dispatch: Dispatch) => {
//         // await deletePlaylist(id);
//         // setPlaylists(await fetchPlaylists());
//         throw new Error("Function not implemented.")
//     }
// }

export function removePlaylists(arg0: { id: string }): any {
    return (dispatch: Dispatch) => {
        // await deletePlaylist(id);
        // setPlaylists(await fetchPlaylists());
        throw new Error("Function not implemented.")
    }
}

export const addNewPlaylist = async (draft: Playlist) => {
    return (dispatch: Dispatch) => {
        // try {
        //     if (!user) {
        //         throw new Error("Not logged in");
        //     }

        //     const saved = await createPlaylist(user.id, draft);
        //     setPlaylists([...playlists, saved]);
        //     return null;
        // } catch (error) {
        //     setMessage(error.message);
        //     return "";
        // }
    }
}

export const savePlaylist = async (draft: Playlist) => {
    return (dispatch: Dispatch) => {
        // await updatePlaylist(draft);
        // setPlaylists(await fetchPlaylists());
        // return null;
    }
};