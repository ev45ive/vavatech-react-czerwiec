import axios from "axios";
import { AnyAction, Reducer } from "redux";
import { AppState } from "../../store";
import { Album, validateSearchResponse } from "../model/Search";

export const featureKey = 'searches'
export interface State {
    results: Album[];
    query: string;
    message: string;
    loading: boolean;
}
export const initialState: State = {
    results: [],
    query: "",
    message: "",
    loading: false,
};


// type SEARCH_START = ReturnType<typeof searchStart>;
type SEARCH_START = { type: "SEARCH_START"; payload: { query: string } };
type SEARCH_SUCCESS = { type: "SEARCH_SUCCESS"; payload: { results: Album[] } };
type SEARCH_FAILED = { type: "SEARCH_FAILED"; payload: { error: Error } };
type Actions = SEARCH_START | SEARCH_SUCCESS | SEARCH_FAILED;

export const reducer: Reducer<State, AnyAction> = (
    state = initialState,
    action
): State => {
    // console.log(action); // Inspect actions

    switch (action.type) {
        case "SEARCH_START":
            return {
                ...state,
                query: action.payload.query,
                loading: true,
                message: "",
                results: [],
            };
        case "SEARCH_SUCCESS":
            return { ...state, loading: false, results: action.payload.results };
        case "SEARCH_FAILED":
            return {
                ...state,
                loading: false,
                message: action.payload.error.message,
            };
        default:
            // Reducer Slice
            // return {...state, substate: subStateReducer(state.substate, action)}
            return state;
    }
}

export function fetchAlbums(query: string | null) {

    // Thunk
    return (dispatch: React.Dispatch<Actions>) => {
        if (!query) return;
        dispatch(searchStart(query));

        axios
            .get("https://api.spotify.com/v1/search", {
                params: { q: query, type: "album" },
            })
            .then(validateSearchResponse<Album>("albums"))
            .then((res) => dispatch(searchSuccess(res)))
            .catch((error) => dispatch(searchFailed(error)));
    }
}

/* Action Creators */

const searchStart = (query: string): SEARCH_START => ({
    type: "SEARCH_START" as const,
    payload: { query },
});

const searchSuccess = (results: Album[]): SEARCH_SUCCESS => ({
    type: "SEARCH_SUCCESS",
    payload: { results },
});

const searchFailed = (error: Error): SEARCH_FAILED => ({
    type: "SEARCH_FAILED",
    payload: { error },
});

/* Selectors */

export const selectSearchFeature = (state: AppState) => state[featureKey]