import axios from "axios";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { UserProfile } from "../model/UserProfile";
import { getToken, logOut } from "../services/AuthService";

interface UserCtx {
  user: UserProfile | null;
  login(): void;
  logout(): void;
}
function missingProvider() {
  throw new Error("Missing UserContext Provider");
}

export const UserContext = React.createContext<UserCtx>({
  user: null,
  login: missingProvider,
  logout: missingProvider,
});

export const UserProvider: React.FC<{}> = ({ children }) => {
  const [user, setUser] = useState<UserProfile | null>(null);

  useEffect(() => {
    if (getToken() && !user) {
      login();
    }
    /* eslint-disable */
  }, []);

  const login = useCallback(async () => {
    const { data: userProfile } = await axios.get<UserProfile>(
      "https://api.spotify.com/v1/me"
    );
    setUser(userProfile);
    // setUser({ display_name: 'Admin' } as UserProfile)
  }, []);

  const logout = useCallback(() => {
    setUser(null);
    logOut();
  }, []);

  return useMemo(
    () => (
      <UserContext.Provider value={{ user, login, logout }}>
        {children}
      </UserContext.Provider>
    ),
    [user]
  );
};

// <UserProvider>
//     <...>
//          <UserWidget/>
//     </...>
// </UserProvider>

export const UserWidget = () => {
  const { user, login, logout } = useContext(UserContext);

  if (!user)
    return (
      <span>
        Welcome Guest | <span onClick={login}>Login</span>
      </span>
    );

  return (
    <span>
      Welcome {user.display_name} | <span onClick={logout}>Logout</span>
    </span>
  );
};
