import axios from "axios"
import { isSpotifyError } from "../model/Search"

const config = {
    auth_url: 'https://accounts.spotify.com/authorize',
    // The client ID provided to you by Spotify when you register your application.
    client_id: 'ddac4e97bfdd4eb79114f53d2b43aac9',
    // Implicit Token flow
    response_type: 'token',
    // The URI to redirect to after the user grants/denies permission. This URI needs to be entered in the URI whitelist that you specify when you register your application.
    redirect_uri: 'http://localhost:3000/',
    // Optional, but strongly recommended. The state can be useful for correlating requests and responses. Because your redirect_uri can be guessed, using a state value can increase your assurance that an incoming connection is the result of an authentication request. If you generate a random string or encode the hash of some client state (e.g., a cookie) in this state variable, you can validate the response to additionally ensure that the request and response originated in the same browser. This provides protection against attacks such as cross-site request forgery. See RFC-6749.
    state: '',
    // A space-separated list of scopes: see Using Scopes.
    scope: [
        'playlist-modify-public',
        'playlist-modify-private',
        'playlist-read-private',
        'playlist-read-collaborative',
    ].join(' '),
}

let token = ''

export const initAuth = () => {
    const access_token = new URLSearchParams(window.location.hash.slice(1)).get('access_token')

    if (access_token) {
        token = access_token
        sessionStorage.setItem('token', JSON.stringify(token))
        window.location.hash = ''
    } else {
        const raw_token = sessionStorage.getItem('token')
        if (raw_token)
            token = JSON.parse(raw_token)
    }

    if (!token) {
        logIn()
    }
}

export const logIn = () => {
    const { auth_url, client_id, response_type, redirect_uri, state, scope } = config

    const params = new URLSearchParams({
        client_id,
        response_type,
        redirect_uri,
        state,
        scope
    })
    const url = `${auth_url}?${params}`
    window.location.href = (url);
}

export const logOut = () => {
    token = ''
    sessionStorage.removeItem('token')
}

export const getToken = () => {
    return token
}


axios.interceptors.request.use(config => {
    config.headers['Authorization'] = `Bearer ${getToken()}`

    return config
})

axios.interceptors.response.use(resp => resp, async error => {

    console.error(error)

    if (!axios.isAxiosError(error)) {
        return Promise.reject(new Error('Unexpected error'))
    }
    if (!error.response) {
        // return axios.request(error.config)
        throw new Error('Cannot connect to server')
    }
    if (error.response.status == 401) {
        setTimeout(() => logIn(), 2000)
        return Promise.reject(new Error('Not logged in'))
    }
    
    if (isSpotifyError(error.response.data)) {
        return Promise.reject(new Error(error.response.data.error.message))
    }
    // Promise.reject('Unexpected response')
    // or
    throw new Error('Unexpected response')
})