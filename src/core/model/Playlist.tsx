import { Track } from "./Search";

export interface Playlist {
    id: string;
    name: string;
    public: boolean;
    description: string;
    tracks?:Track[]
}
