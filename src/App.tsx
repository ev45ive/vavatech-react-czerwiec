import { useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import PlaylistTracksView from "./playlists/containers/PlaylistTracksView";
import { PlaylistsTDDView } from "./playlists/containers/PlaylistsTDDView";
import { NavLink, Redirect, Route, Switch } from "react-router-dom";
import { UserWidget } from "./core/providers/UserProvider";
import { MusicSearchReduxView } from "./music-search/containers/MusicSearchReduxView";
import { Counter } from "./music-search/containers/Counter";
import { PlaylistsReduxView } from "./playlists/containers/PlaylistsReduxView";

function App() {
  const [navbarOpen, setNavbarOpen] = useState(false);

  // useLayoutEffect(() => {
  //     auth.initAuth();
  // }, []);

  return (
    <div className="App">
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <a className="navbar-brand" href="#">
            MusicApp
          </a>

          <button
            className="navbar-toggler"
            type="button"
            aria-controls="navbarNav"
            aria-expanded={navbarOpen}
            aria-label="Toggle navigation"
            onClick={() => setNavbarOpen(!navbarOpen)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div
            className={"collapse navbar-collapse" + navbarOpen ? " show" : ""}
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/search">
                  Search
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="navbar-text ms-auto">
            <UserWidget />
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">
            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />
              <Route path="/playlists" component={PlaylistsReduxView} />
              <Route path="/search" component={MusicSearchReduxView} />
              <Route path="/counter" component={Counter} />
              <Route path="/tracks" component={PlaylistTracksView} />
              <Redirect path="*" to="/search" />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
